<?php

declare(strict_types=1);

namespace DKXTests\NetteGCloud\Tests\Credentials;

use DKX\NetteGCloud\Credentials\FilePathCredentialsLoader;
use DKX\NetteGCloud\Exception\InvalidArgumentException;
use DKXTests\NetteGCloud\TestCase;
use const DIRECTORY_SEPARATOR;

final class FilePathCredentialsLoaderTest extends TestCase
{
	public function testLoadCredentialsNotExists() : void
	{
		$this->expectException(InvalidArgumentException::class);
		$this->expectExceptionMessage('GCP credentials file is missing at "unknown.json"');

		$loader = new FilePathCredentialsLoader('unknown.json');
		$loader->loadCredentials();
	}

	public function testLoadCredentials() : void
	{
		$loader      = new FilePathCredentialsLoader(__DIR__ . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . 'data' . DIRECTORY_SEPARATOR . 'credentials.json');
		$credentials = $loader->loadCredentials();

		self::assertEquals(['project_id' => 'abcd'], $credentials);
	}
}
