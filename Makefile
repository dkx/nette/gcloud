php_image = registry.gitlab.com/dkx/docker/php/cli:7.4.1

.PHONY: deps-install
deps-install:
	docker run --rm -v `pwd`:/app $(php_image) composer install

.PHONY: parallel-lint
parallel-lint:
	docker run --rm -v `pwd`:/app $(php_image) vendor/bin/phplint

.PHONY: phpstan
phpstan:
	docker run --rm -v `pwd`:/app $(php_image) vendor/bin/phpstan analyze --no-progress --no-ansi --memory-limit 1G

.PHONY: cs
cs:
	docker run --rm -v `pwd`:/app $(php_image) vendor/bin/phpcs

.PHONY: cs-fix
cs-fix:
	docker run --rm -v `pwd`:/app $(php_image) vendor/bin/phpcbf

.PHONY: metrics
metrics:
	docker run --rm -v `pwd`:/app $(php_image) php tools/phpmetrics.phar --report-html=phpmetrics --exclude=vendor .

.PHONY: copy-paste-detection
copy-paste-detection:
	docker run --rm -v `pwd`:/app $(php_image) php tools/phpcpd.phar --fuzzy src

.PHONY: loc
phploc:
	docker run --rm -v `pwd`:/app $(php_image) php tools/phploc.phar src tests

.PHONY: psalm
psalm:
	docker run --rm -v `pwd`:/app $(php_image) php tools/psalm.phar

.PHONY: test
test:
	docker run --rm -v `pwd`:/app $(php_image) php tools/phpunit.phar

infection:
	docker run --rm -v `pwd`:/app -e PHP_XDEBUG=1 $(php_image) php tools/infection.phar --threads=4 --no-progress
