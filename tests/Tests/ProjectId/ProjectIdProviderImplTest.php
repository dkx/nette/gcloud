<?php

declare(strict_types=1);

namespace DKXTests\NetteGCloud\Tests\ProjectId;

use DKX\NetteGCloud\ProjectId\ProjectIdLoader;
use DKX\NetteGCloud\ProjectId\ProjectIdProviderImpl;
use DKXTests\NetteGCloud\TestCase;
use Mockery;
use function assert;

final class ProjectIdProviderImplTest extends TestCase
{
	public function testGetProjectId() : void
	{
		$loader = Mockery::mock(ProjectIdLoader::class)
			->shouldReceive('loadProjectId')->once()->andReturn('abcd')->getMock();
		assert($loader instanceof ProjectIdLoader);

		$provider = new ProjectIdProviderImpl($loader);

		$provider->getProjectId();
		$provider->getProjectId();
		$provider->getProjectId();

		self::assertSame('abcd', $provider->getProjectId());
	}
}
