<?php

declare(strict_types=1);

namespace DKX\NetteGCloud\Credentials;

use DKX\NetteGCloud\Exception\InvalidArgumentException;
use Nette\Utils\Json;
use function assert;
use function file_exists;
use function file_get_contents;
use function is_string;

final class FilePathCredentialsLoader implements CredentialsLoader
{
	private string $path;

	public function __construct(string $path)
	{
		$this->path = $path;
	}

	/**
	 * @return mixed[]
	 */
	public function loadCredentials() : array
	{
		if (! file_exists($this->path)) {
			throw new InvalidArgumentException('GCP credentials file is missing at "' . $this->path . '"');
		}

		$credentials = file_get_contents($this->path);
		assert(is_string($credentials));

		return Json::decode($credentials, Json::FORCE_ARRAY);
	}
}
