<?php

declare(strict_types=1);

namespace DKX\NetteGCloud\Credentials;

interface CredentialsLoader
{
	/**
	 * @return mixed[]
	 */
	public function loadCredentials() : array;
}
