<?php

declare(strict_types=1);

namespace DKX\NetteGCloud\ProjectId;

interface ProjectIdProvider
{
	public function getProjectId() : string;
}
