# DKX/NetteGCloud

Google cloud integration for Nette DI

## Installation

```bash
$ composer require dkx/nette-gcloud
```

## Usage

**`config.neon`:**

```yaml
extensions:
    gcloud: DKX\NetteGCloud\DI\GCloudExtension

gcloud:
    credentials:
        loader: DKX\NetteGCloud\Credentials\FilePathCredentialsLoader(%appDir%/../credentials.json)
```

## Extensions
