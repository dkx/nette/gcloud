<?php

declare(strict_types=1);

namespace DKX\NetteGCloud\DI;

use DKX\NetteGCloud\Credentials\CredentialsLoader;
use DKX\NetteGCloud\Credentials\CredentialsProvider;
use DKX\NetteGCloud\Credentials\CredentialsProviderImpl;
use DKX\NetteGCloud\ProjectId\CredentialsProjectIdLoader;
use DKX\NetteGCloud\ProjectId\ProjectIdLoader;
use DKX\NetteGCloud\ProjectId\ProjectIdProvider;
use DKX\NetteGCloud\ProjectId\ProjectIdProviderImpl;
use Nette\DI\CompilerExtension;
use Nette\DI\Definitions\Statement;
use Nette\Schema\Expect;
use Nette\Schema\Schema;
use function assert;
use function is_object;
use function is_string;

/**
 * @codeCoverageIgnore
 */
final class GCloudExtension extends CompilerExtension
{
	public function getConfigSchema() : Schema
	{
		return Expect::structure([
			'credentials' => Expect::structure([
				'loader' => Expect::anyOf(Expect::string(), Expect::type(Statement::class)),
			]),
			'projectId' => Expect::structure([
				'loader' => Expect::anyOf(Expect::string(), Expect::type(Statement::class))
					->default(CredentialsProjectIdLoader::class),
			]),
		]);
	}

	public function loadConfiguration() : void
	{
		$builder = $this->getContainerBuilder();
		$config  = $this->getConfig();
		assert(is_object($config));

		$credentialsLoader = $builder
			->addDefinition($this->prefix('credentials.loader'))
			->setType(CredentialsLoader::class)
			->setAutowired(false);

		if (is_string($config->credentials->loader)) {
			$credentialsLoader->setFactory($config->credentials->loader);
		} elseif ($config->credentials->loader instanceof Statement) {
			$credentialsLoader->setFactory($config->credentials->loader->entity, $config->credentials->loader->arguments);
		}

		$builder
			->addDefinition($this->prefix('credentials.provider'))
			->setType(CredentialsProvider::class)
			->setFactory(CredentialsProviderImpl::class, [$credentialsLoader]);

		$projectIdLoader = $builder
			->addDefinition($this->prefix('projectId.loader'))
			->setType(ProjectIdLoader::class)
			->setAutowired(false);

		if (is_string($config->projectId->loader)) {
			$projectIdLoader->setFactory($config->projectId->loader);
		} elseif ($config->projectId->loader instanceof Statement) {
			$projectIdLoader->setFactory($config->projectId->loader->entity, $config->projectId->loader->arguments);
		}

		$builder
			->addDefinition($this->prefix('projectId.provider'))
			->setType(ProjectIdProvider::class)
			->setFactory(ProjectIdProviderImpl::class, [$projectIdLoader]);
	}
}
