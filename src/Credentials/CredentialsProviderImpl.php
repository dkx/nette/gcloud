<?php

declare(strict_types=1);

namespace DKX\NetteGCloud\Credentials;

final class CredentialsProviderImpl implements CredentialsProvider
{
	private CredentialsLoader $loader;

	/** @var mixed[]|null */
	private ?array $data = null;

	public function __construct(CredentialsLoader $loader)
	{
		$this->loader = $loader;
	}

	/**
	 * @return mixed[]
	 */
	public function getCredentials() : array
	{
		if ($this->data === null) {
			$this->data = $this->loader->loadCredentials();
		}

		return $this->data;
	}
}
