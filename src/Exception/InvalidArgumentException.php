<?php

declare(strict_types=1);

namespace DKX\NetteGCloud\Exception;

final class InvalidArgumentException extends \InvalidArgumentException
{
}
