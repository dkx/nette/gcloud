<?php

declare(strict_types=1);

namespace DKX\NetteGCloud\ProjectId;

final class ProjectIdProviderImpl implements ProjectIdProvider
{
	private ProjectIdLoader $loader;

	private ?string $projectId = null;

	public function __construct(ProjectIdLoader $loader)
	{
		$this->loader = $loader;
	}

	public function getProjectId() : string
	{
		if ($this->projectId === null) {
			$this->projectId = $this->loader->loadProjectId();
		}

		return $this->projectId;
	}
}
