<?php

declare(strict_types=1);

namespace DKX\NetteGCloud\ProjectId;

use DKX\NetteGCloud\Credentials\CredentialsProvider;
use DKX\NetteGCloud\Exception\InvalidArgumentException;
use function array_key_exists;

final class CredentialsProjectIdLoader implements ProjectIdLoader
{
	private CredentialsProvider $credentialsProvider;

	public function __construct(CredentialsProvider $credentialsProvider)
	{
		$this->credentialsProvider = $credentialsProvider;
	}

	public function loadProjectId() : string
	{
		$data = $this->credentialsProvider->getCredentials();

		if (! array_key_exists('project_id', $data)) {
			throw new InvalidArgumentException('GCP credentials is missing the "project_id" key');
		}

		return $data['project_id'];
	}
}
