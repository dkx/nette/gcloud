<?php

declare(strict_types=1);

namespace DKXTests\NetteGCloud\Tests\ProjectId;

use DKX\NetteGCloud\Credentials\CredentialsProvider;
use DKX\NetteGCloud\Exception\InvalidArgumentException;
use DKX\NetteGCloud\ProjectId\CredentialsProjectIdLoader;
use DKXTests\NetteGCloud\TestCase;
use Mockery;
use function assert;

final class CredentialsProjectIdLoaderTest extends TestCase
{
	public function testGetProjectIdNotExists() : void
	{
		$this->expectException(InvalidArgumentException::class);
		$this->expectExceptionMessage('GCP credentials is missing the "project_id" key');

		$credentialsProvider = Mockery::mock(CredentialsProvider::class)
			->shouldReceive('getCredentials')->andReturn([])->getMock();
		assert($credentialsProvider instanceof CredentialsProvider);

		$loader = new CredentialsProjectIdLoader($credentialsProvider);
		$loader->loadProjectId();
	}

	public function testGetProjectId() : void
	{
		$credentialsProvider = Mockery::mock(CredentialsProvider::class)
			->shouldReceive('getCredentials')->andReturn(['project_id' => 'abcd'])->getMock();
		assert($credentialsProvider instanceof CredentialsProvider);

		$loader = new CredentialsProjectIdLoader($credentialsProvider);

		self::assertSame('abcd', $loader->loadProjectId());
	}
}
