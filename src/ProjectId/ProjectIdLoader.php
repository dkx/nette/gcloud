<?php

declare(strict_types=1);

namespace DKX\NetteGCloud\ProjectId;

interface ProjectIdLoader
{
	public function loadProjectId() : string;
}
