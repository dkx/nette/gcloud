<?php

declare(strict_types=1);

namespace DKX\NetteGCloud\Credentials;

interface CredentialsProvider
{
	/**
	 * @return mixed[]
	 */
	public function getCredentials() : array;
}
