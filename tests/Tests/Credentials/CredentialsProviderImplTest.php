<?php

declare(strict_types=1);

namespace DKXTests\NetteGCloud\Tests\Credentials;

use DKX\NetteGCloud\Credentials\CredentialsLoader;
use DKX\NetteGCloud\Credentials\CredentialsProviderImpl;
use DKXTests\NetteGCloud\TestCase;
use Mockery;
use function assert;

final class CredentialsProviderImplTest extends TestCase
{
	public function testGetCredentials() : void
	{
		$credentials = [];

		$loader = Mockery::mock(CredentialsLoader::class)
			->shouldReceive('loadCredentials')->once()->andReturn($credentials)->getMock();
		assert($loader instanceof CredentialsLoader);

		$provider = new CredentialsProviderImpl($loader);

		$provider->getCredentials();
		$provider->getCredentials();
		$provider->getCredentials();

		self::assertSame($credentials, $provider->getCredentials());
	}
}
